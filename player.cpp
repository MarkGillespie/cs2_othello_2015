#include "player.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    mySide = side;
    otherSide = (mySide == BLACK) ? WHITE : BLACK;

    if (mySide == WHITE) {
        CORNER_WEIGHT = 25;
        EDGE_WEIGHT = 10;
    } else {
        CORNER_WEIGHT = 45;
        EDGE_WEIGHT = 5;
    }

    std::cerr<<((mySide == WHITE) ? "WHITE ": "BLACK")<<std::endl;

    if (mySide  == BLACK) {
        numMoves = 0;
    }  else {
        numMoves = 1;
    }

    myBoard = Board();
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/**
 * @brief set the player's internal board
 *
 * @param b the board to give the player
 */
void Player::setBoard(Board *b) {
    myBoard = *b;
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.make minimaxt
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    // std::cerr<<"start"<<std::endl;
    myBoard.doMove(opponentsMove, otherSide);
    // delete opponentsMove;
    numMoves++;
    std::vector<Move *> moves = validMoves();
    if (moves.size() == 0) {
        return NULL;
    } else {
        int maxMinGain = INT_MIN;

        Move *myMove = moves[0];

        // locate 2-turn minimax
        for (std::vector<Move *>::iterator i = moves.begin(); i != moves.end();
            i++) {

            int score = minimax(&myBoard, *i, INT_MIN, INT_MAX, mySide, 3);
            
            std::cerr<<"("<<(*i)->getX()<<","<<(*i)->getY()<<") scored "
            <<score<<std::endl;

            if (score > maxMinGain) {
                maxMinGain = score;
                myMove = *i;
            }
        }


        // copy move so the array can be freed
        myMove = new Move(myMove->getX(), myMove->getY());
        for (unsigned int i = 0; i < moves.size(); i++) {
            delete moves[i];
        }

        // make highest scoring move
        myBoard.doMove(myMove, mySide);
        std::cerr<<"chose ("<<myMove->getX()<<","<<myMove->getY()
            <<") which scored "<<maxMinGain<<std::endl;


        numMoves++;
        return myMove;

    }

}

/**
 * @brief minimizes the maximal loss for mySide using alpha beta pruning
 *
 * @param board the board to evaluate
 * @param m     the move to evaluate
 * @param alpha the best overall score
 * @param beta  the worst overall score
 * @param side  the side making this move
 * @param depth the depth of moves to consider
 *
 * @return value
 */
int Player::minimax(Board *board, Move *m, int alpha, int beta, Side side,
    int depth) {

    int ret;

    Board *newBoard = board->copy();
    newBoard->doMove(m, side);

    if (depth < 1 || m == NULL) {
        return heuristic(newBoard);
    }

    Side otherside = otherside(side);
    std::vector<Move *> moves = validMoves(otherside, newBoard);

    if (side == otherSide) {
        ret = INT_MIN;
        for (std::vector<Move *>::iterator i = moves.begin();
            i != moves.end(); i++) {


            ret = max(ret, minimax(newBoard, *i, alpha, beta, otherSide, 
                depth-1));
            delete *i;
            alpha = max(alpha, ret);
            if (beta < alpha) {
                break;
            }
        }

        moves.clear();

        delete newBoard;
        return ret;

    } else {
        ret = INT_MAX;
        for (std::vector<Move *>::iterator i = moves.begin();
            i != moves.end(); i++) {

            ret = min(ret, minimax(newBoard, *i, alpha, beta, otherSide, 
                depth-1));
            beta = min(beta, ret);
            if (beta < alpha) {
                break;
            }
            delete *i;
        }
        moves.clear();

        delete newBoard;

        return ret;
    }
}

/**
 * @brief heuristic that looks at the state of the board
 *
 * @param b the board to look at
 *
 * @return the value of the board
 */
int Player::heuristic(Board *b) {

    int value;
    int moves[2] = {0, 0};
    getNumMoves(b, moves);

    int cornerBorders[2] = {0, 0};
    getCornerBorders(b, cornerBorders);

    int corners[2] = {0, 0};
    getCorners(b, corners);

    int edges[2] = {0, 0};
    getEdges(b, edges);

    int tiles[2] = {0, 0};
    getTiles(b, tiles);

    // if early in game, minimize number of your tiles 
    if (numMoves < 20) {

        int frontier[2] = {0, 0};
        getFrontier(b, frontier);

        value = moves[0]-moves[1]-tiles[0]+tiles[1]+frontier[1]-frontier[0] +
            CORNER_WEIGHT * (corners[0] - corners[1]) - EDGE_WEIGHT * 
            (edges[0]-edges[1]) + (CORNER_WEIGHT) * 
            (cornerBorders[1] - cornerBorders[0]) * 0.5;
    } else {
        
        int values[2] = {0, 0};

        getBoardValues(b, values);

        value = moves[0]-moves[1] + CORNER_WEIGHT*(corners[0]-corners[1]) +
                EDGE_WEIGHT*(edges[0]-edges[1])+tiles[0]-tiles[1] + 
                (CORNER_WEIGHT) * (cornerBorders[1] - cornerBorders[0]) * 0.5;
    }


    // delete[] moves;

    return value;//controlDifference(b);
}

/**
 * @brief return the minimal gain from a given move after two moves
 *
 * @param possibleMove  a pointer to the move to consider
 *
 * @return the minimal gain after the opponent moves
 */
int Player::testMove(Move *possibleMove) {
    Board *futureBoard = myBoard.copy();
    futureBoard->doMove(possibleMove, mySide);

    int minimalGain = 65; // set it higher than the possible maximum so any
                          // choice will be lower than tested


    std::vector<Move *> possibleReactions = validMoves(otherSide, futureBoard);
    
    for (std::vector<Move *>::iterator i = possibleReactions.begin();
        i != possibleReactions.end(); i++) {

        Board *distantFutureBoard = futureBoard->copy();
        distantFutureBoard->doMove(*i, otherSide);

        int gain = distantFutureBoard->count(mySide);
        if (gain < minimalGain) {
            minimalGain = gain;
        }

        delete distantFutureBoard;
    }

    delete futureBoard;
    return minimalGain;
}


/**
 * @brief finds a list of valid moves that can be played by a given
 * player on a given board
 *
 * @param side  the player whose moves you want to find
 * @param board the board on which you are looking for moves
 *
 * @return the list of moves
 */
std::vector<Move *> Player::validMoves(Side side, Board *board) {
    std::vector<Move *> moves;
    for (int i= 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move *possibleMove = new Move(i, j);
            if (board->checkMove(possibleMove, side)) {
                moves.push_back(possibleMove);
            } else {
                delete possibleMove;
            }
        }
    }
    return moves;
}


/**
 * @brief finds a list of valid moves that can be played this turn
 *
 * @return the list of moves
 */
std::vector<Move *> Player::validMoves() {
    std::vector<Move *> moves;
    for (int i= 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move *possibleMove = new Move(i, j);
            if (myBoard.checkMove(possibleMove, mySide)) {
                moves.push_back(possibleMove);
            } else {
                delete possibleMove;
            }
        }
    }
    return moves;
}

/**
 * @brief finds the number of valid moves that can be played by either
 * player on a given board
 *
 * @param board the board on which you are looking for moves
 * @param arr   the array to store the data to
 */
void Player::getNumMoves(Board *board, int *arr) {
    arr[0] = 0;
    arr[1] = 0;

    std::vector<Move *> moves;
    for (int i= 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move *possibleMove = new Move(i, j);
            if (board->checkMove(possibleMove, mySide)) {
                arr[0]++;
            } else if (board->checkMove(possibleMove, otherSide)) {
                arr[1]++;
            }

            delete possibleMove;
        }
    }
}

/**
 * @brief finds the value of tiles controlled by either player on the board
 *
 * @param board the board you are observing
 * @param arr   the array to store the data to
 */
void Player::getBoardValues(Board *board, int *arr) {
    arr[0] = 0;
    arr[1] = 0;

    std::vector<Move *> moves;
  
    for (int i= 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move *possibleMove = new Move(i, j);
            if (board->checkMove(possibleMove, mySide)) {
                arr[0] += value(possibleMove);
            } else if (board->checkMove(possibleMove, otherSide)) {
                arr[1] += value(possibleMove);
            }

            delete possibleMove;
        }
    }

}

/**
 * @brief finds number of corners controlled by either player on the board
 *
 * @param board the board you are observing
 * @param arr   the array to store the data to
 */
void Player::getCorners(Board *board, int *arr) {
    arr[0] = 0;
    arr[1] = 0;
  
    for (int i= 0; i < 8; i+=7) {
        for (int j = 0; j < 8; j+=7) {
           
            if (board->get(mySide, i, j)) {
                arr[0] ++;
            } else if (board->get(otherSide, i, j)) {
                arr[1] ++;
            }

        }
    }
}

/**
 * @brief finds number of tiles next to corners controlled by either 
 * player on the board
 *
 * @param board the board you are observing
 * @param arr   the array to store the data to
 */
void Player::getCornerBorders(Board *board, int *arr) {
    arr[0] = 0;
    arr[1] = 0;
  
    if (!board->occupied(0, 0)) {
     
        if (board->get(mySide, 1, 1)) {
            arr[0] ++;
        } else if (board->get(otherSide, 1, 1)) {
            arr[1] ++;
        }

        if (board->get(mySide, 0, 1)) {
            arr[0] ++;
        } else if (board->get(otherSide, 0, 1)) {
            arr[1] ++;
        }

        if (board->get(mySide, 1, 0)) {
            arr[0] ++;
        } else if (board->get(otherSide, 1, 0)) {
            arr[1] ++;
        }
    }

    if (!board->occupied (7, 0)) {

        if (board->get(mySide, 6, 1)) {
            arr[0] ++;
        } else if (board->get(otherSide, 6, 1)) {
            arr[1] ++;
        } 

         if (board->get(mySide, 7, 1)) {
            arr[0] ++;
        } else if (board->get(otherSide, 7, 1)) {
            arr[1] ++;
        }

        if (board->get(mySide, 6, 0)) {
            arr[0] ++;
        } else if (board->get(otherSide, 6, 0)) {
            arr[1] ++;
        }
    }
 
    if (!board->occupied(0, 7)) {
        if (board->get(mySide, 1, 6)) {
            arr[0] ++;
        } else if (board->get(otherSide, 1, 6)) {
            arr[1] ++;
        }

        if (board->get(mySide, 1, 7)) {
            arr[0] ++;
        } else if (board->get(otherSide, 1, 7)) {
            arr[1] ++;
        }

        if (board->get(mySide, 0, 6)) {
            arr[0] ++;
        } else if (board->get(otherSide, 0, 6)) {
            arr[1] ++;
        }
    }

    if (!board->occupied(7, 7)) {
        if (board->get(mySide, 6, 6)) {
            arr[0] ++;
        } else if (board->get(otherSide, 6, 6)) {
            arr[1] ++;
        }   

        if (board->get(mySide, 7, 6)) {
            arr[0] ++;
        } else if (board->get(otherSide, 7, 6)) {
            arr[1] ++;
        }

        if (board->get(mySide, 6, 7)) {
            arr[0] ++;
        } else if (board->get(otherSide, 6, 7)) {
            arr[1] ++;
        }
    } 

}

/**
 * @brief finds the number of edges controlled by either player on the board
 *
 * @param board the board you are observing
 * @param arr   the array to store the data to
 */
void Player::getEdges(Board *board, int *arr) {
    arr[0] = 0;
    arr[1] = 0;

    std::vector<Move *> moves;
  
    for (int i= 1; i < 7; i++) {
        for (int j = 0; j < 8; j += 7) {
            if (board->get(mySide, i, j)) {
                arr[0]++;
            } else if (board->get(otherSide, i, j)) {
                arr[1]++;
            }
            if (board->get(mySide, j, i)) {
                arr[0]++;
            } else if (board->get(otherSide, j, i)) {
                arr[1]++;
            }
        }    
    }

}

/**
 * @brief finds the number of tiles controlled by either player on the board
 *
 * @param board the board you are observing
 * @param arr   the array to store the data to
 */
void Player::getTiles(Board *board, int *arr) {
    arr[0] = board->count(mySide);
    arr[1] = board->count(otherSide);

}

/**
 * @brief finds the number of tiles bordering those controlled by either
 * player on the board
 *
 * @param board the board you are observing
 * @param arr   the array to store the data to
 */
void Player::getFrontier(Board *board, int *arr) {
    arr[0] = 0;
    arr[1] = 0;

    std::vector<Move *> moves;
  
    for (int i= 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            if (!board->occupied(i, j)) {
                for (int x = -1; x <= 1; x++)  {
                    for (int y = -1; y <= 1; y++) {
                        if (i+x >= 0 && i + x < 8 && j + y >= 0 && j + y < 8){
                            if (!(x == 0 && y == 0)) {
                                if (board->get(mySide, x+i, y+j)) {
                                    arr[0]++;
                                } else if (board->get(otherSide, x+i, y+j)) {
                                    arr[1]++;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

int Player::value(Move *m) {
    int x = m->getX();
    int y = m->getY();
    if ((x == 0 || x == 7) && (y == 0 || y == 7)) {
        return CORNER_WEIGHT;
    }

    if (x == 0 || x == 7 || y == 0 || y == 7) {
        return EDGE_WEIGHT;
    }

    return 1;
}

/**
 * @brief finds the difference between my squares and the opponent's squares
 *
 * @param b the board to look at
 *
 * @return the value of the board
 */
int Player::controlDifference(Board *b) {
    return b->count(mySide) - b->count(otherSide);
}


