#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <vector>
#include <limits.h>
#include <unistd.h>
     
#define otherside(side) (side == BLACK) ? WHITE : BLACK
#define max(a, b) (a > b) ? a : b
#define min(a, b) (a < b) ? a : b

using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;

    void setBoard(Board *b);


  private:
    Board myBoard;
    Side mySide;
    Side otherSide;
    int numMoves;

    int CORNER_WEIGHT, EDGE_WEIGHT;

    int testMove(Move *possibleMove);
    int minimax(Board *board, Move *m, int alpha, int beta, Side s, int depth);

    int heuristic(Board *b);
    std::vector<Move *> validMoves();
    std::vector<Move *> validMoves(Side side, Board *board);

    void getNumMoves(Board *board, int *arr);
    void getBoardValues(Board *board, int *arr);
    void getFrontier(Board *board, int *arr);
    void getCorners(Board *board, int *arr);
    void getCornerBorders(Board *b, int *arr);
    void getEdges(Board *board, int *arr);
    void getTiles(Board *board, int *arr);
    int value(Move *m);
    int controlDifference(Board *b);
};


#endif
